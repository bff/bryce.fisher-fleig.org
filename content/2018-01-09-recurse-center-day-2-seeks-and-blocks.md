+++
title = "Recurse Center Day 2: Seeks and Blocks"
template = "post.html"
aliases = ["/blog/recurse-center-day-2-seeks-and-blocks/"]

[extra]
excerpt = "Seeks, Blocks, and overview Matroska layout"
+++
I want to learn more about how codecs work so that I one day I can grow up to be a Jedi-master Video Engineer. Here's my notes from Day 2.

## Step 0 - Actually Reading the Specs

The Matroska specs helpfully include a [diagram](https://www.matroska.org/technical/diagram/index.html) which I've ignored up until now which answers about 95% of my questions from last night. To quote the specs:

> The Track section has basic information about each of the tracks. For instance, is it a video, audio or subtitle track? What resolution is the video? What sample rate is the audio? The Track section also says what codec to use to view the track, and has the codec's private data for the track.
>
> The Clusters section has all of the Clusters. These contain all of the video frames and audio for each track.

There's a lot more I learned today, but I'm going to focus on doing it and possibly write down interesting things later.
