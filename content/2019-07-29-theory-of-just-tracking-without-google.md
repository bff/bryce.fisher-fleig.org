+++
title = "Web Analytics without Google - A Just Tracking Theory"
template = "post.html"
aliases = ["/blog/theory-of-just-tracking-without-google/"]

[extra]
excerpt = "Being anti-surveillance is hard to reconcile wanting to start a side hustle, but I think it's possible to do both. Here's my justification for doing web analytics on this site."
+++

I've been thinking about adding some kind of analytics to this website for quite some time, but I've been feeling very conflicted about it. Here's my current thoughts on why tracking might not be evil...at least sometimes!

## Just <s>War</s> Tracking Theory

Although philosophers and statespersons debate the morality of war, marketing people don't ever really consider this in my experience. For philosophers, there are two questions. One, is war _ever_ justifiable? Two, under _what conditions_ is a country justified in going to war? In an age of endless data breaches and tampered elections, I believe a moral theory underlying why it's _ever_ morally permissible to do web analytics is necessary. Without such a theory and conditions under which tracking is morally permissible, content creators run the risk that society will demonize our efforts to share our knowledge and talents with the world.

I have two basic arguments for why I believe tracking is moral under certain conditions.

### The Friendly-Store-Keeper Argument

_A vaguely human shaped bundle of black clothing waddles into your newstand, and puts a gloved hand on a copy of the local newspaper. After leafing through a few pages, the masked bundle sets down the paper and picks up the sports section of a national paper. Finally, the bundle grunts through a voice-hiding mouth-piece and buys a pack of gum at the counter with exact change. You smile politely, ask if they want a receipt, and finally wave farewell as the bundle waddles away._

What's odd about my imaginary story is that we don't expect anywhere near that level of anonymity when conducting business in person offline. But, if there were no web tracking, this is a good analogy for how a website owner would see activity on her site. Just as its normal and not creepy for a shopper keeper to notice my skin color and age when I walk into her store, likewise its reasonable for individual websites to learn _something_ about my visits to their site. For example, I assume a website owner will know what pages I visited, perhaps if I'm a first time visitor or a returning guest, etc.

### The Revenue-By-Other-Means Argument

I really like the Economist. I pay a few hundred dollars a year to receive one issue per week chocked full of British humor and surprising stories from across the world. Last week's top article: Operatic Cross Dressing in China. However, if I didn't pay the Economist, when would the reporters find time to do the research for the paper, or take all the tiny little pictures, or reflect on the sad state of the world in 2019? Answer: they wouldn't. In fact, my subscription alone doesn't even fully compensate all the expenses of the paper, so the Economist (very tastefully) solicits advertising and receives revenue by another means.

Similarly, there are many incredible resources online. If no one pays to use them, there won't be as many. Tracking provides information necessary to convince advertisers to subsidize something valuable to society that is hard to finance otherwise.

### Consumer Espionage

Google Analytics is doing something different. To return to the friendly store keeper analogy, while I think Google should mind their store, I object to their following you down the block, into neighboring stores, and then into your house to stare at your photos. The scope of surveillance from Google is truly staggering -- the microphone in your pocket, the camera in your phone, every email and text message and conference call, your location at every moment and even the questions on your mind as you search for answers.

Building a detailed profile of a person surreptitiously from many sources resembles nothing so much as espionage. While spies secretly collect information, they do it to manipulate, physically harm, or discredit their opponents. Google's goal of manipulating one's purchasing behavior seems much closer to a sort of "consumer" espionage.

Why should you care? Because if you use Google Analytics on your blog or website, you've become a spy for Google by allowing them into your store.

## Be the Friendly Store Keeper You Want To See in the World

How can an individual site owner ensure their tracking is more like a friendly store keeper, and less like consumer espionage? Just don't use Google Analytics, and you'll stop feeding the corporate surveillance machine.

It turns out, at least for smaller blogs and websites, this isn't all that hard to avoid. There are a [handful](https://matomo.org/) [of](https://usefathom.com/) [alternatives](https://goaccess.io/) that you can even run yourself, but any data analytics platform which is not Google is a huge win for privacy.

### With Great Data, Comes Great Responsibility

But, in an age of data breaches and tampered elections, it's not sufficient to avoid surveillance. We have to be responsible for the data we collect. I think we're just starting to think about this as a society, but to me personally it means at least these additional things:

 * Keep <s>your</s> other people's data safe
 * Don't collect more data you have to

I'm not a security expert, but I do think that most security issues stem from a lack of concern about security. If everybody cared just a little bit more to follow basic best practices from the likes of [OWASP](https://www.owasp.org/index.php/Category:Attack) and [NIST](https://www.nist.gov/itl/tig/back-basics-multi-factor-authentication), everyone's data would be a little bit safer.

In terms of minimizing data collection, analytics tools too often dictate this. However, I think its possible to change the status quo and demand more privacy from our analytics tools.

## Next Time

This was the first part in a mini-series on tracking without Google. Next up, what metrics are even worth tracking and a review of alternatives to Google Analytics.
