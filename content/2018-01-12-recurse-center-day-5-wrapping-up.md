+++
title = "Recurse Center Day 5: Wrapping Up"
template = "post.html"
aliases = ["/blog/recurse-center-day-5-wrapping-up/"]

[extra]
excerpt = "My last day at Recurse Center, mini batch 1 2018"
+++
I want to learn more about how video codecs work so someday I can grow up to be a Jedi-master Video Engineer. To aid me on this epic quest, I'm doing the one week "mini-batch" at the [Recurse Center](https://www.recurse.com/). Here's my notes from Day 5.

## Late Night Reflections

Last night, I was chatting with three other programmers from RC and Geoffrey said somethings that got lodged in my head:

### Athletes with no practice

> Can you imagine being an athlete that never practiced and only played real games? And yet that's what we do in software development. We never practice the basic skills.

Then he recounted how one of his friends was practicing how to quickly setup a new angular app from scratch. The friend would schedule 90 minutes to do "reps", and give himself 15 minutes to go from 0 code to a running angular app. At the end of the 15 minutes, he delete the app and start again. Then he'd stop after the 90 minutes were over.

Geoffrey said, "Why don't practice code review and team communication and other skills too? I think I'm going to write a new lisp interpreter every three months to keep practising." I feel like this was one of those watershed moments where the way I think about how to improve has radically altered.

### Failing to learn from failure

One last profound thought from Geoffrey last night:

> Failing to learn from failure is the only unforgivable failure.

While I totally agree with that sentiment, I feel like that's a thought I need to chew on and try to internalize. Reflecting on my own decisions and actions isn't something that comes naturally, but it feels like a skill I ought to focus on.

### Pair Blogging

While chatting with these recursers, I came up with the idea of "pair blogging": it's pair programming except that the "navigator" is responsible to live blog what the pair is programming, code samples, screenshots, problems, design decisions, links to relevant documentation etc. Then when the pair switch roles, the other person writes "Ok, Bob writing now..." and the other person takes over writing. Not sure if it's a terrible idea or an amazing idea...

## Setting Intentions

Today, I'm going to practice timeboxing work. I'll write a repl explorer for an mkv file in 30 minutes, three times (doing "reps"), and then after lunch I'll do other projects: timebox trying to compile to web assembly to an hour, and do three half-hour reps of using bindgen to make rust bindings for libvpx.

## Doing Reps - Making an mkv repl

Doing this project 3 times in a row was just amazing. I never quite got it working all the way, but I've gotten much better at making the repl itself. The difference between rep 1 and rep 2 as immense. I was able to really get good at making the repl itself with multiple commands in an idiomatic Rust fashion. I'm really proud of my work in rep 2.

At the end of rep 2, I had barely started figuring out how to interactively traverse the tree of EMBL Elements, so rep 3 (which was only 15 min) focused on that. I'll need several more reps to figure it out... The whole ownership / `Rc` data structure got in the way. I was fighting the borrow checker the whole. I think, fundamentally I just don't understand how `Rc`'s work at all -- which is really embarrassing after having so much Rust for fun.

See the code from [all three mkv-repl reps on gitlab](https://gitlab.com/bff/reps-mkv-repl).

Honestly, 3 reps was not enough to have making a repl down pat, let alone figure out how to navigate the tree while appeasing the borrow checker. I really want to do this more in the future.

Having a wife and kids who I want and need to spend most of my nonwork hours with, one of the main frustrations I face on a regular basis is that there's nothing meaningful I can do with the scraps of 30 minute blocks of time I have. Now, with reps I have a new way to feel good about doing a mini project on a regular basis that will let me improve and fit into the time I actually have available.

## Compiling Rust to WebAssembly (wasm)

Okay, I've got one hour...where do I even start here? This is _so_ crazy! I remember there was blog from [Rust lang](https://blog.rust-lang.org/) about doing recently. Nope, I can't see any posts about this.. Let's duckduckgo for it. There's a post for Rust 1.14 back in 2016...no way it was that long ago. Uh, yep it was that long ago. Let's find the [really really basic example](https://users.rust-lang.org/t/compiling-to-the-web-with-rust-and-emscripten/7627) of how to do this from the Rust team(s):

> #### Rust installation:
>
> ```
> rustup target add wasm32-unknown-emscripten
> ```
> 
> If you already have rustup installed, then instead of running the curl and
> source commands above, just switch to the nightly toolchain with rustup default nightly.
>
> #### Emscripten installation:
>
> ```
> curl -O https://s3.amazonaws.com/mozilla-games/emscripten/releases/emsdk-portable.tar.gz
> tar -xzf emsdk-portable.tar.gz
> source emsdk_portable/emsdk_env.sh
> emsdk update
> emsdk install sdk-incoming-64bit
> emsdk activate sdk-incoming-64bit
> ```

I have rust already installed, I added toolchain...do I really need to install emscripten separately? Let's just try compiling to the wasm target without installing emscripten:

```
echo 'fn main() { println!("Hello, Emscripten!"); }' > hello.rs
rustc --target=asmjs-unknown-emscripten hello.rs
```

No love. Under the hood, `rustc` is shelling out to `emcc` (presumably the emscripten compiler) which is not found on my system. Ugh, okay, actually install empscripten. Step 1, curl a thing - check. Step 2, untar a thing - check. Step 3, source a thing - nope. There's a typo -- it should be `source emsdk-portable/emsdk_env.sh`. Check. Step 4, update a think, check. Step 4, install the 64 bit sdk, check.....nope, this taking forever. Omg, its still going. Still going. I'm only 55% done and its been running for approximately 35 minutes. Not sure this is going to work out...good thing I have a blog to finish up.

It feels like I should be doing something to help to actually write the code and compile to wasm while I wait...but I don't want to forget to watch for the compilation to finish or I'll not get anywhere in the 19:05 let on my timer.

Let's lookup [how to set the target](https://doc.rust-lang.org/cargo/reference/manifest.html) for a project in the Cargo.toml file. Arg, I can't find anything about how to tell cargo which platform to target. Let's check the `cargo help` command...Great! `cargo build --help` tells that the magic flag is `--target` and I need to put the target triple after that. I know `rustup toolchain list` can tell what the target triple is for wasm...just kidding `rustup target list` it is. The target is `wasm32-unknown-emscripten`...which is what I copied from the rust user forum above. Perfect!

78% of emscripten is build...only 10 minutes left for my hour time box...

What else can I do? I supposed I could start writing something that wraps the mkv crate, but if the hello world code doesn't compile whats the point?

79% compiled....

80% compiled...

Hmm, I guess I could look at code samples that suspected to work with wasm...Okay, here's this [great post about baby steps in Rust -> wasm](https://hackernoon.com/compiling-rust-to-webassembly-guide-411066a69fde) which is the same as I've done (but much better written). It took him 2hrs to compile...so either my compilation is actually _very_ fast, or there's whole other pass after the 81% finished job is done. Anyway, the author suggests enabling a config flag in firefox...let's do that! Here's my about:config default settings:

![Firefox 57 about:config defaults for Web Assembly](/img/2018/firefox-57-aboutconfig-wasm.png)

84% compiled...less than 1 minute to go

Okay, I'll conceded my time is up. Oh well! I knew it was a long shot. Next time I want to try this, I'll have emscripten all ready to go (hopefully!).
