#!/usr/bin/env bash

set -e
set -u

function usage() {
	cat <<- EOF
	./$(basename $0) title [date [excerpt]]
	  * title = required; any alphanumeric string with spaces and punctuation is okay
	  * date = YYYY-MM-DD; defaults to today
	  * excerpt = teaser text on homepage; defaults to empty string

	Creates a new file in _posts/ and fills in the default YAML front matter for
	zola. The title is slugified.
	
	Examples:
	  ./$(basename $0) 'Some mushrooms are magicaler than others'
	  ./$(basename $0) 'Which is best: grSec, AppArmor, and SELinux?' 2017-03-15
	  ./$(basename $0) 'Is TJ really one person?' 2017-02-15 'How I came to learn the truth about TJ (and all millenials)'

	EOF
}

function main() {
	local title="$1"
	local titleSlug=$(echo -n "$title" | tr [:upper:] [:lower:] |tr -s [[:punct:][:space:]*] -)
	local postDate="${2:-$(date +%F)}"
	local excerpt=${3:-}

	cat <<- EOF > "content/$postDate-$titleSlug.md"
+++
title = "$title"
template = "post.html"

[extra]
excerpt = "$excerpt"
+++

EOF
}

if [[ $# -eq 0 ]]; then
	usage
else
	main "${@:-}"
fi
