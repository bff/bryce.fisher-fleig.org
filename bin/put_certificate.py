from collections import namedtuple
from dataclasses import dataclass
from pathlib import Path
from typing import NamedTuple
import argparse
import os
import subprocess
import sys

import requests

CERTBOT_DEFAULT_TIMEOUT = 60


def main(argv):
    if not (gitlab_token := os.getenv('GITLAB_TOKEN')):
        raise ApiError('Missing GITLAB_TOKEN envvar. Make one at https://gitlab.com/-/profile/personal_access_tokens')

    print(f'Raw sys.argv values passed to put_certificate.py: {argv}')
    args = parse_args(argv[1:])
    print('Retrieving new certificates using certbot from LetsyEncrypt server')
    tls_bundle = CertbotRunner(args.email, args.domain).run()
    print(f'Obtained certificates: {tls_bundle}')
    print(f'Uploading certificates to Gitlab for project {args.project_id} as domain {args.domain}')
    gitlab = GitlabClient(args.project_id, gitlab_token)
    result = gitlab.post_certificate(args.domain, tls_bundle)
    print(result)
    return result


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("domain")
    parser.add_argument("email")
    parser.add_argument("project_id")
    return parser.parse_args(argv)


class ApiError(Exception):
    pass


TlsBundle = namedtuple('TlsBundle', ['full_chain', 'private_key'])


@dataclass(eq=True, frozen=True)
class CertbotRunner:
    email: str
    domain: str

    def run(self) -> TlsBundle:
        timeout = os.getenv('CERTBOT_TIMEOUT', CERTBOT_DEFAULT_TIMEOUT)
        try:
            subprocess.run(['certbot', 'certonly', '-n', '--agree-tos', '--email',
                            self.email, '--dns-route53', '-d', self.domain],
                           timeout=timeout, check=True)
        except subprocess.CalledProcessError as e:
            raise ApiError('Failed to obtain certificate from LetsEncrypt server') from e

        return self.tls_bundle()

    def tls_bundle(self):
        return TlsBundle(private_key=Path(f'/etc/letsencrypt/live/{self.domain}/privkey.pem'),
                         full_chain=Path(f'/etc/letsencrypt/live/{self.domain}/fullchain.pem'))


@dataclass(eq=True, frozen=True)
class GitlabClient:
    project_id: int
    token: str

    def post_certificate(self, domain: str, tls_bundle: TlsBundle) -> str:
        endpoint = f"https://gitlab.com/api/v4/projects/{self.project_id}/pages/domains/{domain}"
        response = requests.put(endpoint,
                                 headers={'PRIVATE-TOKEN': self.token},
                                 data={
                                     'certificate': tls_bundle.full_chain.read_text(),
                                     'key': tls_bundle.private_key.read_text(),
                                 })

        if response.status_code == 200:
            return 'Success!'
        elif response.status_code == 403:
            raise ApiError('Gitlab token is revoked or not authorized', response)
        else:
            details = f'''Unexpected response from Gitlab API:
                           - Request URL: {endpoint}
                           - Status code: {response.status_code}
                           - Response headers:
                             {response.headers}

                           - Response text: {response.text}'''
            raise ApiError(details)


if __name__ == '__main__':
    main(sys.argv)
