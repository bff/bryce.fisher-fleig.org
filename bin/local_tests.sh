#!/bin/bash -eux

docker run \
  --rm -it \
  -v "$(pwd)/bin:/opt/blog/bin" \
  --entrypoint "/opt/blog/bin/tests/run_in_docker.sh" \
  certbot/dns-route53
