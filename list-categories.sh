#!/bin/bash

echo "OUTDATED! UPDATE FOR ZOLA"
exit 2

echo Printing Unique Categories in alphanumeric order:
egrep -ho "category: .*" _posts/* | sort -u | tr '[:upper:]' '[:lower:]' | uniq
